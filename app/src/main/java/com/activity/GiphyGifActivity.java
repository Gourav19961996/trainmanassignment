package com.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.adapters.GiphyGifAdapter;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.models.GiphyData;

import com.notifier.ScrollNotifier;
import com.trainman.R;
import com.trainman.databinding.ActivityGiphygifBinding;


import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GiphyGifActivity extends AppCompatActivity implements View.OnClickListener, ScrollNotifier {

    private static final String TAG = GiphyGifActivity.class.getCanonicalName();
    private ActivityGiphygifBinding binding;
    private List<GiphyData.DataBean> gifList = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private GiphyGifAdapter giphyGifAdapter;
    private int currentOffset, pageGifLimit = 25, totalOffset = 250;
    private boolean isNetworkRequestRunning = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_giphygif);

        if (getSupportActionBar() != null) {
            this.getSupportActionBar().hide();
        }

        isNetworkRequestRunning = true;
        requestGifData(0);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        }
    }

    private void requestGifData(final int currentOffset) {
        if (!isNetworkAvailable()) {
            Toast.makeText(this, "No Internet Connection, Try Again Later!", Toast.LENGTH_SHORT).show();
            retriveJsonFromCache(currentOffset);
            return;
        }

        binding.progressBar.setVisibility(View.VISIBLE);

        AndroidNetworking.get("https://api.giphy.com/v1/gifs/trending?api_key=dHIDPPfTtJ2jTwTQgQaD3S5hjd1WSlSZ&limit="+pageGifLimit+"&rating=g")
                .addQueryParameter("offset", String.valueOf(currentOffset))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        GiphyData giphyData = GiphyData.fromJSon(response);
                        isNetworkRequestRunning = false;
                        if (giphyGifAdapter == null) {
                            initRecyclerView(giphyData);
                            binding.progressBar.setVisibility(View.INVISIBLE);
                        } else {
                            gifList.addAll(giphyData.getData());
                            binding.rvGiphyGif.post(new Runnable() {
                                @Override
                                public void run() {
                                    giphyGifAdapter.notifyDataSetChanged();
                                }
                            });
                            binding.progressBar.setVisibility(View.INVISIBLE);
                        }

                        try {
                            FileWriter file = new FileWriter("/data/data/" + getApplicationContext().getPackageName() + "/" + currentOffset);
                            file.write(response.toString());
                            file.flush();
                            file.close();
                        } catch (IOException e) {
                            Log.d(TAG, "onResponseException: "+e.getMessage());
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, "onError: " + anError.getErrorBody());
                    }
                });

    }

    private void retriveJsonFromCache(int currentOffset) {
        String mResponse = null;
        try {
            File f = new File("/data/data/" + getPackageName() + "/" + currentOffset);
            if(!f.exists())
                return;
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
             mResponse = new String(buffer);
        } catch (IOException e) {
            Log.d(TAG, "retriveJsonFromCacheException: " + e.getMessage());
            e.printStackTrace();
        }
        GiphyData giphyData = GiphyData.fromString(mResponse);
        if (giphyGifAdapter == null) {
            initRecyclerView(giphyData);
            binding.progressBar.setVisibility(View.INVISIBLE);
        } else {
            gifList.addAll(giphyData.getData());
            binding.rvGiphyGif.post(new Runnable() {
                @Override
                public void run() {
                    giphyGifAdapter.notifyDataSetChanged();
                }
            });
            binding.progressBar.setVisibility(View.INVISIBLE);
        }

    }

    private void initRecyclerView(GiphyData giphyData) {
        layoutManager = new GridLayoutManager(GiphyGifActivity.this, 2);
        binding.rvGiphyGif.setLayoutManager(layoutManager);
        gifList.addAll(giphyData.getData());
        giphyGifAdapter = new GiphyGifAdapter(this, this, gifList);
        binding.rvGiphyGif.setAdapter(giphyGifAdapter);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onScrolledToBottom() {
        currentOffset = currentOffset + pageGifLimit;

        if (currentOffset < totalOffset) {
            Log.d("currentOffset: " + currentOffset, "totalOffset: " + totalOffset);
            requestGifData(currentOffset);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}