package com.models;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.List;

public class GiphyData {
    @SerializedName("pagination")
    private PaginationBean pagination;
    @SerializedName("meta")
    private MetaBean meta;
    @SerializedName("data")
    private List<DataBean> data;

    public static GiphyData fromJSon(JSONObject response) {
        return new Gson().fromJson(String.valueOf(response), GiphyData.class);
    }

    public static GiphyData fromString(String response) {
        return new Gson().fromJson(response, GiphyData.class);
    }

    public PaginationBean getPagination() {
        return pagination;
    }

    public void setPagination(PaginationBean pagination) {
        this.pagination = pagination;
    }

    public MetaBean getMeta() {
        return meta;
    }

    public void setMeta(MetaBean meta) {
        this.meta = meta;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class PaginationBean {
        @SerializedName("total_count")
        private int totalCount;
        @SerializedName("count")
        private int count;
        @SerializedName("offset")
        private int offset;

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }
    }

    public static class MetaBean {
        @SerializedName("status")
        private int status;
        @SerializedName("msg")
        private String msg;
        @SerializedName("response_id")
        private String responseId;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getResponseId() {
            return responseId;
        }

        public void setResponseId(String responseId) {
            this.responseId = responseId;
        }
    }

    public static class DataBean {
        @SerializedName("type")
        private String type;
        @SerializedName("id")
        private String id;
        @SerializedName("url")
        private String url;
        @SerializedName("slug")
        private String slug;
        @SerializedName("bitly_gif_url")
        private String bitlyGifUrl;
        @SerializedName("bitly_url")
        private String bitlyUrl;
        @SerializedName("embed_url")
        private String embedUrl;
        @SerializedName("username")
        private String username;
        @SerializedName("source")
        private String source;
        @SerializedName("title")
        private String title;
        @SerializedName("rating")
        private String rating;
        @SerializedName("content_url")
        private String contentUrl;
        @SerializedName("source_tld")
        private String sourceTld;
        @SerializedName("source_post_url")
        private String sourcePostUrl;
        @SerializedName("is_sticker")
        private int isSticker;
        @SerializedName("import_datetime")
        private String importDatetime;
        @SerializedName("trending_datetime")
        private String trendingDatetime;
        @SerializedName("images")
        private ImagesBean images;
        @SerializedName("user")
        private UserBean user;
        @SerializedName("analytics_response_payload")
        private String analyticsResponsePayload;
        @SerializedName("analytics")
        private AnalyticsBean analytics;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getBitlyGifUrl() {
            return bitlyGifUrl;
        }

        public void setBitlyGifUrl(String bitlyGifUrl) {
            this.bitlyGifUrl = bitlyGifUrl;
        }

        public String getBitlyUrl() {
            return bitlyUrl;
        }

        public void setBitlyUrl(String bitlyUrl) {
            this.bitlyUrl = bitlyUrl;
        }

        public String getEmbedUrl() {
            return embedUrl;
        }

        public void setEmbedUrl(String embedUrl) {
            this.embedUrl = embedUrl;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getContentUrl() {
            return contentUrl;
        }

        public void setContentUrl(String contentUrl) {
            this.contentUrl = contentUrl;
        }

        public String getSourceTld() {
            return sourceTld;
        }

        public void setSourceTld(String sourceTld) {
            this.sourceTld = sourceTld;
        }

        public String getSourcePostUrl() {
            return sourcePostUrl;
        }

        public void setSourcePostUrl(String sourcePostUrl) {
            this.sourcePostUrl = sourcePostUrl;
        }

        public int getIsSticker() {
            return isSticker;
        }

        public void setIsSticker(int isSticker) {
            this.isSticker = isSticker;
        }

        public String getImportDatetime() {
            return importDatetime;
        }

        public void setImportDatetime(String importDatetime) {
            this.importDatetime = importDatetime;
        }

        public String getTrendingDatetime() {
            return trendingDatetime;
        }

        public void setTrendingDatetime(String trendingDatetime) {
            this.trendingDatetime = trendingDatetime;
        }

        public ImagesBean getImages() {
            return images;
        }

        public void setImages(ImagesBean images) {
            this.images = images;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public String getAnalyticsResponsePayload() {
            return analyticsResponsePayload;
        }

        public void setAnalyticsResponsePayload(String analyticsResponsePayload) {
            this.analyticsResponsePayload = analyticsResponsePayload;
        }

        public AnalyticsBean getAnalytics() {
            return analytics;
        }

        public void setAnalytics(AnalyticsBean analytics) {
            this.analytics = analytics;
        }

        public static class ImagesBean {
            @SerializedName("original")
            private OriginalBean original;
            @SerializedName("downsized")
            private DownsizedBean downsized;
            @SerializedName("downsized_large")
            private DownsizedLargeBean downsizedLarge;
            @SerializedName("downsized_medium")
            private DownsizedMediumBean downsizedMedium;
            @SerializedName("downsized_small")
            private DownsizedSmallBean downsizedSmall;
            @SerializedName("downsized_still")
            private DownsizedStillBean downsizedStill;
            @SerializedName("fixed_height")
            private FixedHeightBean fixedHeight;
            @SerializedName("fixed_height_downsampled")
            private FixedHeightDownsampledBean fixedHeightDownsampled;
            @SerializedName("fixed_height_small")
            private FixedHeightSmallBean fixedHeightSmall;
            @SerializedName("fixed_height_small_still")
            private FixedHeightSmallStillBean fixedHeightSmallStill;
            @SerializedName("fixed_height_still")
            private FixedHeightStillBean fixedHeightStill;
            @SerializedName("fixed_width")
            private FixedWidthBean fixedWidth;
            @SerializedName("fixed_width_downsampled")
            private FixedWidthDownsampledBean fixedWidthDownsampled;
            @SerializedName("fixed_width_small")
            private FixedWidthSmallBean fixedWidthSmall;
            @SerializedName("fixed_width_small_still")
            private FixedWidthSmallStillBean fixedWidthSmallStill;
            @SerializedName("fixed_width_still")
            private FixedWidthStillBean fixedWidthStill;
            @SerializedName("looping")
            private LoopingBean looping;
            @SerializedName("original_still")
            private OriginalStillBean originalStill;
            @SerializedName("original_mp4")
            private OriginalMp4Bean originalMp4;
            @SerializedName("preview")
            private PreviewBean preview;
            @SerializedName("preview_gif")
            private PreviewGifBean previewGif;
            @SerializedName("preview_webp")
            private PreviewWebpBean previewWebp;
            @SerializedName("hd")
            private HdBean hd;
            @SerializedName("480w_still")
            private _$480wStillBean $480wStill;

            public OriginalBean getOriginal() {
                return original;
            }

            public void setOriginal(OriginalBean original) {
                this.original = original;
            }

            public DownsizedBean getDownsized() {
                return downsized;
            }

            public void setDownsized(DownsizedBean downsized) {
                this.downsized = downsized;
            }

            public DownsizedLargeBean getDownsizedLarge() {
                return downsizedLarge;
            }

            public void setDownsizedLarge(DownsizedLargeBean downsizedLarge) {
                this.downsizedLarge = downsizedLarge;
            }

            public DownsizedMediumBean getDownsizedMedium() {
                return downsizedMedium;
            }

            public void setDownsizedMedium(DownsizedMediumBean downsizedMedium) {
                this.downsizedMedium = downsizedMedium;
            }

            public DownsizedSmallBean getDownsizedSmall() {
                return downsizedSmall;
            }

            public void setDownsizedSmall(DownsizedSmallBean downsizedSmall) {
                this.downsizedSmall = downsizedSmall;
            }

            public DownsizedStillBean getDownsizedStill() {
                return downsizedStill;
            }

            public void setDownsizedStill(DownsizedStillBean downsizedStill) {
                this.downsizedStill = downsizedStill;
            }

            public FixedHeightBean getFixedHeight() {
                return fixedHeight;
            }

            public void setFixedHeight(FixedHeightBean fixedHeight) {
                this.fixedHeight = fixedHeight;
            }

            public FixedHeightDownsampledBean getFixedHeightDownsampled() {
                return fixedHeightDownsampled;
            }

            public void setFixedHeightDownsampled(FixedHeightDownsampledBean fixedHeightDownsampled) {
                this.fixedHeightDownsampled = fixedHeightDownsampled;
            }

            public FixedHeightSmallBean getFixedHeightSmall() {
                return fixedHeightSmall;
            }

            public void setFixedHeightSmall(FixedHeightSmallBean fixedHeightSmall) {
                this.fixedHeightSmall = fixedHeightSmall;
            }

            public FixedHeightSmallStillBean getFixedHeightSmallStill() {
                return fixedHeightSmallStill;
            }

            public void setFixedHeightSmallStill(FixedHeightSmallStillBean fixedHeightSmallStill) {
                this.fixedHeightSmallStill = fixedHeightSmallStill;
            }

            public FixedHeightStillBean getFixedHeightStill() {
                return fixedHeightStill;
            }

            public void setFixedHeightStill(FixedHeightStillBean fixedHeightStill) {
                this.fixedHeightStill = fixedHeightStill;
            }

            public FixedWidthBean getFixedWidth() {
                return fixedWidth;
            }

            public void setFixedWidth(FixedWidthBean fixedWidth) {
                this.fixedWidth = fixedWidth;
            }

            public FixedWidthDownsampledBean getFixedWidthDownsampled() {
                return fixedWidthDownsampled;
            }

            public void setFixedWidthDownsampled(FixedWidthDownsampledBean fixedWidthDownsampled) {
                this.fixedWidthDownsampled = fixedWidthDownsampled;
            }

            public FixedWidthSmallBean getFixedWidthSmall() {
                return fixedWidthSmall;
            }

            public void setFixedWidthSmall(FixedWidthSmallBean fixedWidthSmall) {
                this.fixedWidthSmall = fixedWidthSmall;
            }

            public FixedWidthSmallStillBean getFixedWidthSmallStill() {
                return fixedWidthSmallStill;
            }

            public void setFixedWidthSmallStill(FixedWidthSmallStillBean fixedWidthSmallStill) {
                this.fixedWidthSmallStill = fixedWidthSmallStill;
            }

            public FixedWidthStillBean getFixedWidthStill() {
                return fixedWidthStill;
            }

            public void setFixedWidthStill(FixedWidthStillBean fixedWidthStill) {
                this.fixedWidthStill = fixedWidthStill;
            }

            public LoopingBean getLooping() {
                return looping;
            }

            public void setLooping(LoopingBean looping) {
                this.looping = looping;
            }

            public OriginalStillBean getOriginalStill() {
                return originalStill;
            }

            public void setOriginalStill(OriginalStillBean originalStill) {
                this.originalStill = originalStill;
            }

            public OriginalMp4Bean getOriginalMp4() {
                return originalMp4;
            }

            public void setOriginalMp4(OriginalMp4Bean originalMp4) {
                this.originalMp4 = originalMp4;
            }

            public PreviewBean getPreview() {
                return preview;
            }

            public void setPreview(PreviewBean preview) {
                this.preview = preview;
            }

            public PreviewGifBean getPreviewGif() {
                return previewGif;
            }

            public void setPreviewGif(PreviewGifBean previewGif) {
                this.previewGif = previewGif;
            }

            public PreviewWebpBean getPreviewWebp() {
                return previewWebp;
            }

            public void setPreviewWebp(PreviewWebpBean previewWebp) {
                this.previewWebp = previewWebp;
            }

            public HdBean getHd() {
                return hd;
            }

            public void setHd(HdBean hd) {
                this.hd = hd;
            }

            public _$480wStillBean get$480wStill() {
                return $480wStill;
            }

            public void set$480wStill(_$480wStillBean $480wStill) {
                this.$480wStill = $480wStill;
            }

            public static class OriginalBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;
                @SerializedName("mp4_size")
                private String mp4Size;
                @SerializedName("mp4")
                private String mp4;
                @SerializedName("webp_size")
                private String webpSize;
                @SerializedName("webp")
                private String webp;
                @SerializedName("frames")
                private String frames;
                @SerializedName("hash")
                private String hash;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getMp4Size() {
                    return mp4Size;
                }

                public void setMp4Size(String mp4Size) {
                    this.mp4Size = mp4Size;
                }

                public String getMp4() {
                    return mp4;
                }

                public void setMp4(String mp4) {
                    this.mp4 = mp4;
                }

                public String getWebpSize() {
                    return webpSize;
                }

                public void setWebpSize(String webpSize) {
                    this.webpSize = webpSize;
                }

                public String getWebp() {
                    return webp;
                }

                public void setWebp(String webp) {
                    this.webp = webp;
                }

                public String getFrames() {
                    return frames;
                }

                public void setFrames(String frames) {
                    this.frames = frames;
                }

                public String getHash() {
                    return hash;
                }

                public void setHash(String hash) {
                    this.hash = hash;
                }
            }

            public static class DownsizedBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class DownsizedLargeBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class DownsizedMediumBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class DownsizedSmallBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("mp4_size")
                private String mp4Size;
                @SerializedName("mp4")
                private String mp4;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getMp4Size() {
                    return mp4Size;
                }

                public void setMp4Size(String mp4Size) {
                    this.mp4Size = mp4Size;
                }

                public String getMp4() {
                    return mp4;
                }

                public void setMp4(String mp4) {
                    this.mp4 = mp4;
                }
            }

            public static class DownsizedStillBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class FixedHeightBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;
                @SerializedName("mp4_size")
                private String mp4Size;
                @SerializedName("mp4")
                private String mp4;
                @SerializedName("webp_size")
                private String webpSize;
                @SerializedName("webp")
                private String webp;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getMp4Size() {
                    return mp4Size;
                }

                public void setMp4Size(String mp4Size) {
                    this.mp4Size = mp4Size;
                }

                public String getMp4() {
                    return mp4;
                }

                public void setMp4(String mp4) {
                    this.mp4 = mp4;
                }

                public String getWebpSize() {
                    return webpSize;
                }

                public void setWebpSize(String webpSize) {
                    this.webpSize = webpSize;
                }

                public String getWebp() {
                    return webp;
                }

                public void setWebp(String webp) {
                    this.webp = webp;
                }
            }

            public static class FixedHeightDownsampledBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;
                @SerializedName("webp_size")
                private String webpSize;
                @SerializedName("webp")
                private String webp;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getWebpSize() {
                    return webpSize;
                }

                public void setWebpSize(String webpSize) {
                    this.webpSize = webpSize;
                }

                public String getWebp() {
                    return webp;
                }

                public void setWebp(String webp) {
                    this.webp = webp;
                }
            }

            public static class FixedHeightSmallBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;
                @SerializedName("mp4_size")
                private String mp4Size;
                @SerializedName("mp4")
                private String mp4;
                @SerializedName("webp_size")
                private String webpSize;
                @SerializedName("webp")
                private String webp;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getMp4Size() {
                    return mp4Size;
                }

                public void setMp4Size(String mp4Size) {
                    this.mp4Size = mp4Size;
                }

                public String getMp4() {
                    return mp4;
                }

                public void setMp4(String mp4) {
                    this.mp4 = mp4;
                }

                public String getWebpSize() {
                    return webpSize;
                }

                public void setWebpSize(String webpSize) {
                    this.webpSize = webpSize;
                }

                public String getWebp() {
                    return webp;
                }

                public void setWebp(String webp) {
                    this.webp = webp;
                }
            }

            public static class FixedHeightSmallStillBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class FixedHeightStillBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class FixedWidthBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;
                @SerializedName("mp4_size")
                private String mp4Size;
                @SerializedName("mp4")
                private String mp4;
                @SerializedName("webp_size")
                private String webpSize;
                @SerializedName("webp")
                private String webp;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getMp4Size() {
                    return mp4Size;
                }

                public void setMp4Size(String mp4Size) {
                    this.mp4Size = mp4Size;
                }

                public String getMp4() {
                    return mp4;
                }

                public void setMp4(String mp4) {
                    this.mp4 = mp4;
                }

                public String getWebpSize() {
                    return webpSize;
                }

                public void setWebpSize(String webpSize) {
                    this.webpSize = webpSize;
                }

                public String getWebp() {
                    return webp;
                }

                public void setWebp(String webp) {
                    this.webp = webp;
                }
            }

            public static class FixedWidthDownsampledBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;
                @SerializedName("webp_size")
                private String webpSize;
                @SerializedName("webp")
                private String webp;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getWebpSize() {
                    return webpSize;
                }

                public void setWebpSize(String webpSize) {
                    this.webpSize = webpSize;
                }

                public String getWebp() {
                    return webp;
                }

                public void setWebp(String webp) {
                    this.webp = webp;
                }
            }

            public static class FixedWidthSmallBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;
                @SerializedName("mp4_size")
                private String mp4Size;
                @SerializedName("mp4")
                private String mp4;
                @SerializedName("webp_size")
                private String webpSize;
                @SerializedName("webp")
                private String webp;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getMp4Size() {
                    return mp4Size;
                }

                public void setMp4Size(String mp4Size) {
                    this.mp4Size = mp4Size;
                }

                public String getMp4() {
                    return mp4;
                }

                public void setMp4(String mp4) {
                    this.mp4 = mp4;
                }

                public String getWebpSize() {
                    return webpSize;
                }

                public void setWebpSize(String webpSize) {
                    this.webpSize = webpSize;
                }

                public String getWebp() {
                    return webp;
                }

                public void setWebp(String webp) {
                    this.webp = webp;
                }
            }

            public static class FixedWidthSmallStillBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class FixedWidthStillBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class LoopingBean {
                @SerializedName("mp4_size")
                private String mp4Size;
                @SerializedName("mp4")
                private String mp4;

                public String getMp4Size() {
                    return mp4Size;
                }

                public void setMp4Size(String mp4Size) {
                    this.mp4Size = mp4Size;
                }

                public String getMp4() {
                    return mp4;
                }

                public void setMp4(String mp4) {
                    this.mp4 = mp4;
                }
            }

            public static class OriginalStillBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class OriginalMp4Bean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("mp4_size")
                private String mp4Size;
                @SerializedName("mp4")
                private String mp4;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getMp4Size() {
                    return mp4Size;
                }

                public void setMp4Size(String mp4Size) {
                    this.mp4Size = mp4Size;
                }

                public String getMp4() {
                    return mp4;
                }

                public void setMp4(String mp4) {
                    this.mp4 = mp4;
                }
            }

            public static class PreviewBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("mp4_size")
                private String mp4Size;
                @SerializedName("mp4")
                private String mp4;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getMp4Size() {
                    return mp4Size;
                }

                public void setMp4Size(String mp4Size) {
                    this.mp4Size = mp4Size;
                }

                public String getMp4() {
                    return mp4;
                }

                public void setMp4(String mp4) {
                    this.mp4 = mp4;
                }
            }

            public static class PreviewGifBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class PreviewWebpBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class HdBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("mp4_size")
                private String mp4Size;
                @SerializedName("mp4")
                private String mp4;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getMp4Size() {
                    return mp4Size;
                }

                public void setMp4Size(String mp4Size) {
                    this.mp4Size = mp4Size;
                }

                public String getMp4() {
                    return mp4;
                }

                public void setMp4(String mp4) {
                    this.mp4 = mp4;
                }
            }

            public static class _$480wStillBean {
                @SerializedName("height")
                private String height;
                @SerializedName("width")
                private String width;
                @SerializedName("size")
                private String size;
                @SerializedName("url")
                private String url;

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getSize() {
                    return size;
                }

                public void setSize(String size) {
                    this.size = size;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }
        }

        public static class UserBean {
            @SerializedName("avatar_url")
            private String avatarUrl;
            @SerializedName("banner_image")
            private String bannerImage;
            @SerializedName("banner_url")
            private String bannerUrl;
            @SerializedName("profile_url")
            private String profileUrl;
            @SerializedName("username")
            private String username;
            @SerializedName("display_name")
            private String displayName;
            @SerializedName("description")
            private String description;
            @SerializedName("instagram_url")
            private String instagramUrl;
            @SerializedName("website_url")
            private String websiteUrl;
            @SerializedName("is_verified")
            private boolean isVerified;

            public String getAvatarUrl() {
                return avatarUrl;
            }

            public void setAvatarUrl(String avatarUrl) {
                this.avatarUrl = avatarUrl;
            }

            public String getBannerImage() {
                return bannerImage;
            }

            public void setBannerImage(String bannerImage) {
                this.bannerImage = bannerImage;
            }

            public String getBannerUrl() {
                return bannerUrl;
            }

            public void setBannerUrl(String bannerUrl) {
                this.bannerUrl = bannerUrl;
            }

            public String getProfileUrl() {
                return profileUrl;
            }

            public void setProfileUrl(String profileUrl) {
                this.profileUrl = profileUrl;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public String getDisplayName() {
                return displayName;
            }

            public void setDisplayName(String displayName) {
                this.displayName = displayName;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getInstagramUrl() {
                return instagramUrl;
            }

            public void setInstagramUrl(String instagramUrl) {
                this.instagramUrl = instagramUrl;
            }

            public String getWebsiteUrl() {
                return websiteUrl;
            }

            public void setWebsiteUrl(String websiteUrl) {
                this.websiteUrl = websiteUrl;
            }

            public boolean isIsVerified() {
                return isVerified;
            }

            public void setIsVerified(boolean isVerified) {
                this.isVerified = isVerified;
            }
        }

        public static class AnalyticsBean {
            @SerializedName("onload")
            private OnloadBean onload;
            @SerializedName("onclick")
            private OnclickBean onclick;
            @SerializedName("onsent")
            private OnsentBean onsent;

            public OnloadBean getOnload() {
                return onload;
            }

            public void setOnload(OnloadBean onload) {
                this.onload = onload;
            }

            public OnclickBean getOnclick() {
                return onclick;
            }

            public void setOnclick(OnclickBean onclick) {
                this.onclick = onclick;
            }

            public OnsentBean getOnsent() {
                return onsent;
            }

            public void setOnsent(OnsentBean onsent) {
                this.onsent = onsent;
            }

            public static class OnloadBean {
                @SerializedName("url")
                private String url;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class OnclickBean {
                @SerializedName("url")
                private String url;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }

            public static class OnsentBean {
                @SerializedName("url")
                private String url;

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }
        }
    }
}
