package com.extras;

import android.app.Application;
import android.content.Context;

import com.androidnetworking.AndroidNetworking;

public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        AndroidNetworking.initialize(getApplicationContext());
    }

}
