package com.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.activity.GiphyGifActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.listeners.GifClickListener;
import com.models.GiphyData;
import com.notifier.ScrollNotifier;
import com.trainman.R;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import pl.droidsonroids.gif.GifImageView;


public class GiphyGifAdapter extends RecyclerView.Adapter<GiphyGifAdapter.GiphyGifHolder> {

    private Context context;
    private List<GiphyData.DataBean> gifList = new ArrayList<>();
    private ScrollNotifier scrollNotifier;

    public GiphyGifAdapter(Context context, ScrollNotifier scrollNotifier, List<GiphyData.DataBean> gifList) {
        this.context = context;
        this.gifList = gifList;
        this.scrollNotifier = scrollNotifier;
    }

    @NonNull
    @Override
    public GiphyGifAdapter.GiphyGifHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.gif_container, parent, false);
        return new GiphyGifAdapter.GiphyGifHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GiphyGifHolder holder, int position) {

        GiphyData.DataBean dataBean = gifList.get(position);
        Glide.with(context)
                .load(dataBean.getImages().getPreviewGif().getUrl())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(holder.ivGiphyGif);
        holder.ivGiphyGif.setOnClickListener(new GifClickListener(context, dataBean.getImages().getPreviewGif().getUrl()));

        if (position == gifList.size() - 1)
            scrollNotifier.onScrolledToBottom();

    }

    @Override
    public int getItemCount() {
        return gifList.size();
    }

    public class GiphyGifHolder extends RecyclerView.ViewHolder {

        private ImageView ivGiphyGif;

        public GiphyGifHolder(@NonNull View itemView) {
            super(itemView);

            ivGiphyGif = itemView.findViewById(R.id.ivGiphyGif);
        }
    }
}
