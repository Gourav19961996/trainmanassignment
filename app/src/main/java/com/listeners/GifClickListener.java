package com.listeners;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.trainman.R;

public class GifClickListener implements View.OnClickListener{

    private Context context;
    private String url;

    public GifClickListener(Context context, String url) {
        this.context = context;
        this.url = url;
    }

    @Override
    public void onClick(View view) {
        createZoomDialog();
    }

    public void createZoomDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.gif_zoom_dialog_layout, null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ImageView ivZoomedGif = view.findViewById(R.id.ivZoomedGif);
        Glide.with(context).load(url).into(ivZoomedGif);
    }


}
